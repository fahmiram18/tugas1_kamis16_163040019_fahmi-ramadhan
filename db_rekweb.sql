-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2018 at 06:43 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_rekweb`
--
CREATE DATABASE IF NOT EXISTS `db_rekweb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_rekweb`;

-- --------------------------------------------------------

--
-- Table structure for table `otomotif`
--

DROP TABLE IF EXISTS `otomotif`;
CREATE TABLE IF NOT EXISTS `otomotif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(64) NOT NULL,
  `jenis` varchar(64) NOT NULL,
  `merk` varchar(64) NOT NULL,
  `kapasitas_mesin` int(11) NOT NULL,
  `tahun_edar` year(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otomotif`
--

INSERT INTO `otomotif` (`id`, `nama`, `jenis`, `merk`, `kapasitas_mesin`, `tahun_edar`) VALUES
(1, 'RC-213V GP', 'Motor', 'Honda', 1000, 2018),
(2, 'YZR-M1 GP', 'Motor', 'Yamaha', 1000, 2018),
(15, 'Civic', 'Mobil', 'Honda', 1500, 2018),
(17, 'Celica', 'Mobil', 'Toyota', 1800, 2006);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
