<?php 

/**
 * 
 */
class Otomotif extends Controller
{
	public function index()
	{
		$keyword = " ";
		if (isset($_POST['s'])) {
			$keyword = '%'.$_POST['s'].'%';
			$model = $this->model('Otomotif_model');
			$this->view('templates/header', $data = ['judul' => 'Daftar Otomotif']);
			$this->view('otomotif/index', $data = ['oto' => $model->getSearch($keyword)]);
			$this->view('templates/footer');
			
		} else {
			$model = $this->model('Otomotif_model');
			$this->view('templates/header', $data = ['judul' => 'Daftar Otomotif']);
			$this->view('otomotif/index', $data = ['oto' => $model->getAllOto()]);
			$this->view('templates/footer');
		}
	}

	public function detail()
	{
		$id = $_POST['id'];
		$model = $this->model('Otomotif_model');
		$data = $model->getOtoById($id);
		echo json_encode($data);
	}

	public function tambah()
	{
		if (isset($_POST['submit'])) {
			$data['nama'] = $_POST['nama'];
			$data['jenis'] = $_POST['jenis'];
			$data['merk'] = $_POST['merk'];
			$data['kapasitas_mesin'] = $_POST['kapasitas_mesin'];
			$data['tahun_edar'] = $_POST['tahun_edar'];
			$model = $this->model('Otomotif_model');
			$model->tambah($data);
			header("Location: otomotif/index");
		} else {
			$model = $this->model('Otomotif_model');
			$this->view('templates/header', $data = ['judul' => 'Daftar Otomotif']);
			$this->view('otomotif/index', $data = ['oto' => $model->getAllOto()]);
			$this->view('templates/footer');
		}

	}

	public function hapus($id)
	{	
		$model = $this->model('Otomotif_model');
		$model->hapus($id);
		header("Location: http://localhost/tugas1_kamis16_163040019_fahmi-ramadhan/public/otomotif");
	}

	public function ubah()
	{
		if (isset($_POST['submit'])) {
			$data['id'] = $_POST['id'];
			$data['nama'] = $_POST['nama'];
			$data['jenis'] = $_POST['jenis'];
			$data['merk'] = $_POST['merk'];
			$data['kapasitas_mesin'] = $_POST['kapasitas_mesin'];
			$data['tahun_edar'] = $_POST['tahun_edar'];
			$model = $this->model('Otomotif_model');
			$model->ubah($data);
			header("Location: otomotif/index");
		} else {
			$model = $this->model('Otomotif_model');
			$this->view('templates/header', $data = ['judul' => 'Daftar Otomotif']);
			$this->view('otomotif/index', $data = ['oto' => $model->getAllOto()]);
			$this->view('templates/footer');
		}

	}

	// public function cari()
	// {
	// 	if (isset($_POST['s'])) {
	// 		$keyword = $_POST['s'];
	// 		$model = $this->model('Otomotif_model');
	// 		$this->view('templates/header', $data = ['judul' => 'Daftar Otomotif']);
	// 		$this->view('otomotif/index', $data = ['oto' => $model->cari($keyword)]);
	// 		$this->view('templates/footer');
			
	// 	} else {
	// 		$model = $this->model('Otomotif_model');
	// 		$this->view('templates/header', $data = ['judul' => 'Daftar Otomotif']);
	// 		$this->view('otomotif/index', $data = ['oto' => $model->getAllOto()]);
	// 		$this->view('templates/footer');
	// 	}
	// }
}