	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="<?php echo BASEURL ?>/js/materialize.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo BASEURL ?>/js/ajaxSearch.js"></script> -->
    <script>
    	const sideNav = document.querySelectorAll('.sidenav');
      	M.Sidenav.init(sideNav);

      	const modalTambah = document.querySelectorAll('#tambah');
      	M.Modal.init(modalTambah);
        
        const modalUbah = document.querySelectorAll('#ubah');
      	M.Modal.init(modalUbah);

      	function handleClickUpdate(id) {
  			$.ajax({
                  type: "POST",
                  url: 'http://localhost/tugas1_kamis16_163040019_fahmi-ramadhan/public/otomotif/detail',
                  data: "id=" + id,
                  success: function(data) {
                  	var a = JSON.parse(data);
                  	$("#ubah_id").val(id);
                  	$("#tampil_id").val(id);
                    $("#ubah_nama").val(a.nama);
                    $("#ubah_jenis").val(a.jenis);
                    $("#ubah_merk").val(a.merk);
                    $("#ubah_kapasitas_mesin").val(a.kapasitas_mesin);
                    $("#ubah_tahun_edar").val(a.tahun_edar);
                  }
              });
  		  };
    </script>
</body>
</html>