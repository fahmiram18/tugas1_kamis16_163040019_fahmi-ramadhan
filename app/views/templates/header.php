<!DOCTYPE html>
<html lang="en">
<head>
	<!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo BASEURL ?>/css/materialize.min.css"  media="screen,projection"/>
    
    <!-- My CSS -->
    <link rel="stylesheet" href="">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo $data['judul'] ?></title>
</head>
<body>

<div class="navbar-fixed" style="margin-bottom: 10px;">
	<nav class="grey darken-4">
		<div class="nav-wrapper container">
			<a href="#" class="brand-logo">Logo</a>
            <a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      		<ul id="nav-mobile" class="right hide-on-med-and-down">
        		<li><a href="<?php echo BASEURL ?>/otomotif">Beranda</a></li>
      		</ul>
    	</div>
  	</nav>
</div>

<ul class="sidenav" id="mobile-nav">
    <li><a href="<?php echo BASEURL ?>/otomotif">Beranda</a></li>
</ul>