<div class="container">
	<div class="row">
		<div class="col s6 left" style="margin-top: 15px;">
			<a class="waves-effect waves-light btn modal-trigger green" href="#tambah">
				Tambah Data<i class="material-icons right">exposure_plus_1</i>
			</a>
		</div>
		<div class="col m4 s6 right">
			<form action="<?php echo BASEURL ?>/otomotif" method="post">
				<input id="s" name="s" type="text" placeholder="Cari....">
		        <!-- <div class="input-field">
		          	<i class="material-icons prefix">search</i>
		          	<input id="s" name="s" type="text">
		          	<label for="s">Cari...</label>
		        </div> -->
		    </form>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<table class="striped highlight responsive-table">
				<thead class="grey darken-4 white-text">
					<tr>
						<th class="center">#</th>
						<th>Nama</th>
						<th>Jenis</th>
						<th>Merk</th>
						<th>Kapasitas Mesin</th>
						<th>Tahun Edar</th>
						<th class="center"><i class="material-icons prefix">settings</i></th>
					</tr>
				</thead>
				<tbody>
					<?php if (empty($data['oto'])) { ?>
						<tr>
							<td colspan="7" class="center">
								Maaf, Data Tidak Ditemukan.
							</td>
						</tr>
					<?php } else { ?>
					<?php $i = 1; ?>
					<?php foreach ($data['oto'] as $oto) : ?>
						<tr>
							<td class="center"><?= $i ?></td>
							<td><?= $oto['nama']; ?></td>
							<td><?= $oto['jenis']; ?></td>
							<td><?= $oto['merk']; ?></td>
							<td><?= $oto['kapasitas_mesin']; ?>cc</td>
							<td><?= $oto['tahun_edar']; ?></td>
							<td class="center">
								<a href="<?php echo BASEURL ?>/otomotif/ubah/<?= $oto['id']; ?>" onclick="handleClickUpdate(<?= $oto['id']; ?>)">
									<button class="btn-small orange modal-trigger" href="#ubah">Ubah
									</button>
								</a>
								<a class="btn-small red" href="<?php echo BASEURL ?>/otomotif/hapus/<?php echo $oto['id'] ?>" onclick="return confirm('Ketika dihapus, data tidak dapat dikembalikan. Lanjutkan?')">
									Hapus
								</a>
							</td>
						</tr>
					<?php $i++; endforeach ?>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- Modal Structure -->
<div id="tambah" class="modal modal-fixed-footer">
	<div class="modal-content">
		<form action="<?php echo BASEURL ?>/otomotif/tambah" method="post">
		    <h4>Tambah Data</h4>
	    	<div class="input-field">
				<input id="nama" type="text" name="nama" autofocus>
				<label for="nama">Nama Kendaraan</label>
	        </div>
	        <div class="input-field">
				<input id="jenis" type="text" name="jenis">
				<label for="jenis">Jenis Kendaraan</label>
	        </div>
	        <div class="input-field">
				<input id="merk" type="text" name="merk">
				<label for="merk">Merk Kendaraan</label>
	        </div>
	        <div class="input-field">
				<input id="kapasitas_mesin" type="number" name="kapasitas_mesin">
				<label for="kapasitas_mesin">Kapasitas Mesin</label>
	        </div>
	        <div class="input-field">
				<input id="tahun_edar" type="number" name="tahun_edar">
				<label for="tahun_edar">Tahun Edar</label>
	        </div>
	    </div>
	    <div class="modal-footer">
	    	<button type="submit" name="submit" id="submit" class="modal-close waves-effect waves-green btn"><i class="material-icons">save</i></button>
	    	<a href="<?php echo BASEURL ?>/otomotif" class="modal-close waves-effect waves-green btn red"><i class="material-icons" onclick="return confirm('Ketika kembali, data tidak akan disimpan. Lanjutkan?')">close</i></a>
	    </div>
    </form>
</div>

<!-- Modal Ubah -->
<div id="ubah" class="modal modal-fixed-footer">
	<div class="modal-content">
		<form action="<?php echo BASEURL ?>/otomotif/ubah" method="post">
			<input id="ubah_id" type="hidden" name="id" value="<?= $oto['id']; ?>">
		    <h4>Ubah Data</h4>
		    <div class="input-field">
				<input id="tampil_id" type="number" disabled value="<?= $oto['id']; ?>">
				<label for="id">ID</label>
	        </div>
	    	<div class="input-field">
				<input id="ubah_nama" type="text" name="nama" value="<?= $oto['nama']; ?>" autofocus>
				<label for="nama">Nama Kendaraan</label>
	        </div>
	        <div class="input-field">
				<input id="ubah_jenis" type="text" name="jenis" value="<?= $oto['nama']; ?>">
				<label for="jenis">Jenis Kendaraan</label>
	        </div>
	        <div class="input-field">
				<input id="ubah_merk" type="text" name="merk" value="<?= $oto['nama']; ?>">
				<label for="merk">Merk Kendaraan</label>
	        </div>
	        <div class="input-field">
				<input id="ubah_kapasitas_mesin" type="number" name="kapasitas_mesin" value="<?= $oto['nama']; ?>">
				<label for="kapasitas_mesin">Kapasitas Mesin</label>
	        </div>
	        <div class="input-field">
				<input id="ubah_tahun_edar" type="number" name="tahun_edar" value="<?= $oto['nama']; ?>">
				<label for="tahun_edar">Tahun Edar</label>
	        </div>
		</div>
		<div class="modal-footer">
		   	<button type="submit" name="submit" id="submit" class="modal-close waves-effect waves-green btn"><i class="material-icons">save</i></button>
		   	<a href="<?php echo BASEURL ?>/otomotif" class="modal-close waves-effect waves-green btn red"><i class="material-icons" onclick="return confirm('Ketika kembali, data tidak akan disimpan. Lanjutkan?')">close</i></a>
		</div>
	</form>
</div>