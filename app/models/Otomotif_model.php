<?php 

/**
 * 
 */
class Otomotif_model
{
	// attributes
	private $table = 'otomotif';
	private $db;
	private $nama;
	private $jenis;
	private $merk;
	private $kapasitas_mesin;
	private $tahun_edar;

	// construct
	function __construct()
	{
		$this->db = new Database;
	}

	// methods
	public function getAllOto()
	{
		$this->db->query('SELECT * FROM ' . $this->table);
		return $this->db->resultSet();
	}

	public function getSearch($keyword)
	{
		$this->db->query("SELECT * FROM " . $this->table . " WHERE nama LIKE :keyword OR jenis LIKE :keyword OR merk LIKE :keyword OR kapasitas_mesin LIKE :keyword OR tahun_edar LIKE :keyword");
		$this->db->bind('keyword', $keyword);
		return $this->db->resultSet();
	}

	public function getOtoById($id)
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=:id');
		$this->db->bind('id', $id);
		return $this->db->single();
	}

	public function tambah($data)
	{
		$nama = $data['nama'];
		$jenis = $data['jenis'];
		$merk = $data['merk'];
		$kapasitas_mesin = $data['kapasitas_mesin'];
		$tahun_edar = $data['tahun_edar'];

		$query = "INSERT INTO " . $this->table .
						" VALUES ('',:nama,:jenis,:merk,:kapasitas_mesin,:tahun_edar)";
		$this->db->query($query);
		$this->db->bind('nama', $nama);
		$this->db->bind('jenis', $jenis);
		$this->db->bind('merk', $merk);
		$this->db->bind('kapasitas_mesin', $kapasitas_mesin);
		$this->db->bind('tahun_edar', $tahun_edar);
		$this->db->execute();
	}

	public function hapus($id)
	{
		$this->db->query('DELETE FROM ' . $this->table . ' WHERE id=:id');
		$this->db->bind('id', $id);
		$this->db->execute();
	}

	public function ubah($data)
	{
		$id = $data['id'];
		$nama = $data['nama'];
		$jenis = $data['jenis'];
		$merk = $data['merk'];
		$kapasitas_mesin = $data['kapasitas_mesin'];
		$tahun_edar = $data['tahun_edar'];

		$query = "UPDATE " . $this->table . "
					SET nama = :nama, jenis = :jenis, merk = :merk, kapasitas_mesin = :kapasitas_mesin, tahun_edar = :tahun_edar
					WHERE id =:id";
		$this->db->query($query);
		$this->db->bind('id', $id);
		$this->db->bind('nama', $nama);
		$this->db->bind('jenis', $jenis);
		$this->db->bind('merk', $merk);
		$this->db->bind('kapasitas_mesin', $kapasitas_mesin);
		$this->db->bind('tahun_edar', $tahun_edar);
		$this->db->execute();
	}

	// public function cari($keyword)
	// {
	// 	$this->db->query("SELECT * FROM " . $this->table . " WHERE nama LIKE '%$keyword%' OR jenis LIKE '%$keyword%' OR merk LIKE '%$keyword%' OR kapasitas_mesin LIKE '%$keyword%' OR tahun_edar LIKE '%$keyword%'");
	// 	$this->db->bind('keyword', $keyword);
	// 	return $this->db->resultSet();
	// }

}